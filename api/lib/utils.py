# -*- coding:utf-8 -*-
import json
import ObjectWeb

def json_in():
    data = ObjectWeb.context["requestvars"].value
    return json.loads(data)

def json_out(data):
    return json.dumps(data)

def load_file(path):
    with open(path) as f:
        endpoints = json.load(f)
    return endpoints
