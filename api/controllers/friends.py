# -*- coding:utf-8 -*-
import ObjectWeb

from api.lib.db import r_server
from api.lib.utils import json_in


class AddFriend(object):
    def POST(self):
        ObjectWeb.header("Content-Type", "application/json")
        data = json_in()
        starting_account_id = data['starting_account_id']
        ending_account_id = data['ending_account_id']

        r_server.sadd("accounts:%s:friends" % starting_account_id,
                      ending_account_id)
        r_server.sadd("accounts:%s:friends" % ending_account_id,
                      starting_account_id)


class RemoveFriend(object):
    def POST(self):
        ObjectWeb.header("Content-Type", "application/json")
        data = json_in()
        starting_account_id = data['starting_account_id']
        ending_account_id = data['ending_account_id']

        r_server.srem("accounts:%s:friends" % starting_account_id,
                      ending_account_id)
        r_server.srem("accounts:%s:friends" % ending_account_id,
                      starting_account_id)


class Stats(object):
    pass