# -*- coding:utf-8 -*-
import datetime

import ObjectWeb

from api.lib.utils import json_in, json_out
from api.lib.db import r_server


class Friend(object):

    def GET(self, account_id):
        ObjectWeb.header("Content-Type", "application/json")
        friends = r_server.smembers("accounts:%s:friends" % account_id)
        accounts = []
        for id in friends:
            account = r_server.hgetall('accounts:' + str(id))

            accounts.append({'account_id': account.get('account_id'),
                             'name': account.get('name'),
                             'url': account.get('url')})
        return json_out(accounts)

    def POST(self, account_id):
        ObjectWeb.header("Content-Type", "application/json")


class AccountNode(object):

    def GET(self, account_id):
        ObjectWeb.header("Content-Type", "application/json")
        account = r_server.hgetall('accounts:' + str(account_id))
        return json_out(account)

    def PUT(self, account_id):
        ObjectWeb.header("Content-Type", "application/json")
        pass


class Account(object):
    ACCOUNTS_PAGE = 10

    def GET(self, page=1):
        ObjectWeb.header("Content-Type", "application/json")
        accounts = self._get_page(int(page))
        return json_out(accounts)

    def POST(self):
        ObjectWeb.header("Content-Type", "application/json")
        data = json_in()
        now = datetime.datetime.now()

        r_server.incr("accounts:counter")
        ctr = r_server.get("accounts:counter")

        account = {
            'account_id': ctr,
            'name': data['name'],
            'url': 'http://localhost:8000/api/v1.0/accounts/' + ctr,
            'friend_collection_url': 'http://localhost:8000/api/v1.0/accounts/',
            'created_at': now.strftime("%Y-%m-%dT%H:%M:%S"),
            'updated_at': ''
        }

        r_server.hmset("accounts:" + ctr, account)
        return json_out(data['name'])

    def _get_page(self, page):
        val_max = self.ACCOUNTS_PAGE * page
        val_min = val_max - self.ACCOUNTS_PAGE + 1

        accounts = []
        for id in range(val_min, val_max + 1):
            account = r_server.hgetall('accounts:' + str(id))
            if not account:
                break

            accounts.append({'account_id': account.get('account_id'),
                             'name': account.get('name'),
                             'url': account.get('url')})

        return accounts