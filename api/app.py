# -*- coding:utf-8 -*-
import ObjectWeb

from api.resources.accounts import Account, AccountNode, Friend
from api.controllers.friends import AddFriend, RemoveFriend, Stats
from api.lib.utils import json_out, load_file

endpoints = load_file('config.json')


class MainPage(object):

    def GET(self):
        ObjectWeb.header("Content-Type", "application/json")
        return json_out(endpoints)


class E404Page(object):

    def GET(self):
        ObjectWeb.header("Content-Type", "application/json")
        ObjectWeb.status("404 Not Found")

        return {'error': '404 Not Found'}


app = ObjectWeb.Application({
    "/api/v1.0/": MainPage,

    "/api/v1.0/accounts": Account,
    "/api/v1.0/accounts/([0-9_]+)": AccountNode,
    "/api/v1.0/accounts/([0-9]+)/friends": Friend,

    "/api/v1.0/add_friend": AddFriend,
    "/api/v1.0/remove_friend": RemoveFriend,
    "/api/v1.0/stats": Stats,

    "HTTP-404": E404Page,
})

if __name__ == "__main__":
    app.run("localhost", 8000)
else:
    application = app.getwsgi()